# bwinf38-1-Urlaubsfahrt

Bearbeitung der Aufgabe4: Nummernmerker der ersten Runde des 38. BWINF 2019/20
https://bwinf.de/bundeswettbewerb/38/1-runde/

## Vorgehensweisen

1. Als erstes die minimal mögliche Anzahl an Tankstellen herausfinden -> minT

2. Wenn ein Weg mehr als minT Tankstellen anfährt, dann kann er verworfen werden.

3. Am Ziel sollte der Tank leer sein, um Kosten zu sparen.

## Algorithmus



## Begriffe

minT = minimal nötige Anzahl an Tankstellen auf der Strecke = minStops
minC = minimale Kosten von einer Tanksetlle aus zum Ziel

## Ideen

Man muss entscheiden, zu welchen Tanksetllen man alles fährt
Und an jeder Tankstelle wie viel man Tankt:
Komplett voll oder gerade genug

--> kann man eigentlich ziemlich schön rekursiv lösen...
--> also auf jeden Fall auch dynamisch -> Zwischenergebnisse speichern

Umsetzung...

Vielleicht muss ich alles nochmal neu überdenken...
Er rechnet schon bei der fahrt2.txt ewig rum,
weil er an 10 Stations kommt und all möglichen Kombinationen von auslassen separat berechnen muss...
Gibt es was Schnelleres oder eine Optimierungs-möglichkeit?
Ich hab das Gefühl, dass es wesentlich einfacher gehen kann
z.B Zur billigsten Tankstelle innerhalb der Reichweite zu fahren...

Noch ne andere Idee:
Einen Weg mit der minimalen Tankstellenzahl finden
und dann seine Stationen wiederholt nach links oder rechts (zur billigsten Tankstelle) verschieben
-> main2.ts

ABER: im Normalfall ist nur die letzte Station vershiebbar.
Wenn die aber bereits in ihrem scheinbar optimalen Platz ist und sich so nicht mehr verändert, können günstigere Alternativen weiter vorne nicht entdeckt werden!
-> Alle möglichen Routen finden, aber auf der gleichen Weise wie oben



Weil du denn Gedanken immer wieder hast:
Es geht NICHT, immer die billigste erreichbare Tankstelle zu nehmen und von dort aus weiterzufahren,
weil auf diese Weise die minimale Anzahl an Stops nicht beachtet wird.