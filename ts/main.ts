import { uploadFileAsync, LoadedFile } from "./files.js";

interface Data {
	filename: string,
	/** Verbrauch */
	v: number,
	/** Tankgröße */
	g: number,
	/** Start Tankfüllung */
	f: number,
	/** Länge der Strecke */
	l: number,
	stations: Station[];
}

interface Db {
	paths: Path[],
	CACHE: {
		calcSectionCost: Map<string, number>,
		getAlternatives: Map<string, Station[]>
	}
}

interface Station {
	id: number,
	d: number,
	p: number
}
interface Stop {
	station: Station,
	tankingUp: number,
	cost: number
}
interface Route {
	cost: number,
	stops: Stop[];
}
interface Path {
	stations: Station[],
	done?: boolean // ? brauch ich noch?
}

const DATA: Partial<Data> = {};
const DB: Db = {
	paths: [],
	CACHE: {
		calcSectionCost: new Map(),
		getAlternatives: new Map()
	}
};

async function start() {
	// Get Input
	let fileBody = await uploadFileAsync();
	extractData(fileBody);
	console.log(DATA);

	// Run
	console.time("Laufzeit");
	let res = main();
	console.timeEnd("Laufzeit");

	// Display Output
	output(res);
}
document.getElementById("start").onclick = start;

function main(): Route {
	let initialPath = makeInitialPath();
	let pathVariations = [initialPath, ...makePathVariations(initialPath)];
	console.log("pathsMade, #: ", pathVariations.length);
	let routeVariations = pathVariations.map(p => pathToRoute(p));
	let bestRoute = routeVariations.reduce((a, b) => a.cost < a.cost ? a : b);
	return bestRoute;
}

var counter = 0;
function makePathVariations(initialPath: Path, startWith: number=0): Path[] {
	// temp
	counter++;
	if (counter % 1000 === 0) console.log("Counter:", counter);

	let path = initialPath;

	const startStation: Station = {id: -1,                   d: 0,      p: Infinity};
	const goalStation : Station = {id: DATA.stations.length, d: DATA.l, p: 0       };

	for (let pi = startWith ; pi < path.stations.length; pi++) {
		const current = path.stations[pi];
		const prev = path.stations[pi-1] || startStation;
		const next = path.stations[pi+1] || goalStation;

		// * ich brauch hier nur nach links zu gehen mit den Stations, weil alle veränderungen rechts schon durch andere alternatives gedeckt sind
		let alternatives: Station[] = getAlternatives(current, prev, next);

		let foundPaths = alternatives.map(a => {
			let p = copyPath(path);
			p.stations[pi] = a;
			return p;
		});

		let newPaths = foundPaths.filter(fp => !DB.paths.some(np => pathsAreEqual(fp, np)));
		DB.paths.push(...newPaths);

		// ! temp
		if (newPaths.length > 0) console.log(DB.paths.length);
		if (newPaths.some(p => !checkPossibility(p)) || DB.paths.length>5000) {console.log(newPaths); throw new Error("Impossible path found")}

		for (const p of newPaths) {
			// * kann man so rekursiv machen, weil ich jedes mal nur nach links gehe,
			// * und wenn es nicht weiter nach links geht, dann gibt es keine newPaths und der Loop hier läuft gar nicht
			makePathVariations(p, Math.max(0, pi-1));
		}
	}
	return DB.paths;
}

// ! temp
function checkPossibility(path: Path) {
	let s = path.stations;
	for (let i = 0 ; i < s.length-1; i++) {
		if (!isInRange(s[i].d, s[i+1].d)) return false;
	}
	return true;
}


var cacheCounter = 0;
function getAlternatives(current: Station, prev: Station, next: Station): Station[] {
	let hash = current.id + "-" + next.id + "-" + prev.id;
	if (DB.CACHE.getAlternatives.has(hash)) {

		// ! temp
		cacheCounter++;
		if (cacheCounter % 1000 === 0) console.log("cachedCounter:", cacheCounter);

		return DB.CACHE.getAlternatives.get(hash);
	}
	
	let alternatives: Station[] = [];
	for (let si = current.id-1 ; si > prev.id; si--) {
		const alt = DATA.stations[si];
		if (isInRange(alt.d, next.d)) {
			alternatives.push(alt);
		} else break;
	}
	console.log("hier", hash, alternatives.length);
	
	DB.CACHE.getAlternatives.set(hash, alternatives);
	return alternatives;
}


function pathToRoute(path: Path): Route {
	const startStation: Station = {id: -1,                   d: 0,      p: Infinity};
	const goalStation : Station = {id: DATA.stations.length, d: DATA.l, p: 0       };

	let s = path.stations;
	let stops: Stop[] = [];
	// ! tank kann negativ werden
	let tank = DATA.f;
	tank -= distanceToFuel(s[0].d);
	for (let i = 0 ; i < s.length; i++) {
		const current = s[i];
		const prev = s[i-1] || startStation;
		const next = s[i+1] || goalStation;
		
		// ! distanceAhead wird zu groß -> unmögliche Pfade kommen iwo her -> makePathVariation
		let distanceAhead = next.d - current.d;
		// ! fuelNeeded kann größer als DATA.g werden
		let fuelNeeded = distanceToFuel(distanceAhead);
		console.log(tank, fuelNeeded, distanceAhead);
		
		let tankUpToFull = next.p > current.p;
		let tankingUp: number;
		if (tankUpToFull) {
			tankingUp = DATA.g - tank;
			// for next stop
			tank = DATA.g - fuelNeeded;
		} else {
			tankingUp = Math.max(fuelNeeded - tank, 0);
			// for next stop
			tank = 0;
		}
		let cost = tankingUp * current.p;
		let stop: Stop = {station: current, tankingUp, cost};
		stops.push(stop);
	}
	let totalCost = stops.reduce((c, s) => c+s.cost, 0);
	let route: Route = {stops, cost: totalCost};
	return route;
}


function makeInitialPath(): Path {
	// * Hier muss ne gescheite Kopie hin, da sonst die Reihenfolge in DATA.stations auch reversed wird
	const reversedStations = DATA.stations.slice();
	reversedStations.reverse();
	const stations: Station[] = [];
	
	let pos = 0;
	let endOfRange: number = fuelToDistance(DATA.f);
	let lastStation: Station;
	do {
		lastStation = reversedStations.find(s => s.d < endOfRange);
		stations.push(lastStation);
		pos = lastStation.d;
		endOfRange = pos + fuelToDistance(DATA.g);
		
		// until end is reachable
	} while (DATA.l > endOfRange);

	return {stations};
}


// === MINI HELPERS ===
function fuelToDistance(fuel: number): number {
	return 100 * fuel / DATA.v;
}
function distanceToFuel(distance: number): number {
	return (distance/100) * DATA.v;
}
function isReachable(point: number, from: number): boolean {
	return (point > from && point <= from + fuelToDistance(DATA.g));
}
function isInRange(start: number, end: number): boolean {
	return (end > start && end <= (start + fuelToDistance(DATA.g)));
}
function pathsAreEqual(a: Path, b: Path): boolean {
	for (let pi = 0 ; pi < a.stations.length; pi++) {
		if (a.stations[pi].id !== b.stations[pi].id) {
			return false;
		}
	}
	return true;
}
function copyPath(path: Path): Path {
	let newPath: Path = {stations: []};
	for (let i = 0 ; i < path.stations.length; i++) {
		newPath.stations[i] = path.stations[i];
	}
	return newPath;
}
function calcSectionCost(stations: Station[]): number {
	let hash = stations.reduce((h, s) => h+s.id+"-", "");
	if (DB.CACHE.calcSectionCost.has(hash)) return DB.CACHE.calcSectionCost.get(hash); 
	let cost = 0;
	for (let i = 0 ; i < stations.length-1; i++) {
		let distance = stations[i+1].d - stations[i].d;
		cost += distanceToFuel(distance) * stations[i].p;
	}
	DB.CACHE.calcSectionCost.set(hash, cost);
	return cost;
}


// I/O
function output(res: Route) {
	let table = res.stops.map((s, i) => ({"#Stop": i, "#Station": s.station.id, stationPos: s.station.d, getankt: s.tankingUp.toFixed(2), kosten: formatCost(s.cost)}));
	let out = buildOutput(res);
	let html = out.replace(/\n/g, "<br>") + json2table(table);
	document.write(html);
	console.log(out);
	console.table(table)

}
function buildOutput(res: Route) {
	return (`\
=== Ergebnis für ${DATA.filename} ===
Anzahl Stops: ${res.stops.length}
Gesamtkosten: ${formatCost(res.cost)}
Stops:
`);
}
function json2table(json: Object[], classes: string = "") {
	// strongly edited from https://travishorn.com/building-json2table-turn-json-into-an-html-table-a57cf642b84a
	let cols = Object.keys(json[0]);

	let headerRow = '';
	let bodyRows = '';

	function capitalizeFirstLetter(string: string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	cols.map(col => {
		headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
	});
	json.map(row => {
		bodyRows += '<tr>';
		cols.map(colName => {
			bodyRows += '<td>' + row[colName] + '</td>';
		})
		bodyRows += '</tr>';
	});

	return `<table class="${classes}"><thead><tr>${headerRow}</tr></thead><tbody>${bodyRows}</tbody></table>`;
}
function formatCost(cost: number) {
	return (cost/100).toFixed(2) + " €";
}

function extractData(file: LoadedFile) {
	DATA.filename = file.name;
	let lines = file.body.split(/\r?\n/g);

	DATA.v = Number(lines[0]);
	DATA.g = Number(lines[1]);
	DATA.f = Number(lines[2]);
	DATA.l = Number(lines[3]);

	let stationLines = lines.slice(5, -1);
	let stations = stationLines.map((sl) => {
		let numbers = sl.split(/ +/).map(s => Number(s));
		return {d: numbers[0], p: numbers[1]};
	});
	stations = stations.sort((a, b) => a.d - b.d);
	DATA.stations = stations.map((s, id) => ({id, ...s}));
}
