var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { uploadFileAsync } from "./files.js";
const DATA = {};
const DB = {
    paths: [],
    CACHE: {
        calcSectionCost: new Map(),
        getAlternatives: new Map()
    }
};
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        // Get Input
        let fileBody = yield uploadFileAsync();
        extractData(fileBody);
        console.log(DATA);
        // Run
        console.time("Laufzeit");
        let res = main();
        console.timeEnd("Laufzeit");
        // Display Output
        output(res);
    });
}
document.getElementById("start").onclick = start;
function main() {
    let initialPath = makeInitialPath();
    let pathVariations = [initialPath, ...makePathVariations(initialPath)];
    console.log("pathsMade, #: ", pathVariations.length);
    let routeVariations = pathVariations.map(p => pathToRoute(p));
    let bestRoute = routeVariations.reduce((a, b) => a.cost < a.cost ? a : b);
    return bestRoute;
}
var counter = 0;
function makePathVariations(initialPath, startWith = 0) {
    // temp
    counter++;
    if (counter % 1000 === 0)
        console.log("Counter:", counter);
    let path = initialPath;
    const startStation = { id: -1, d: 0, p: Infinity };
    const goalStation = { id: DATA.stations.length, d: DATA.l, p: 0 };
    for (let pi = startWith; pi < path.stations.length; pi++) {
        const current = path.stations[pi];
        const prev = path.stations[pi - 1] || startStation;
        const next = path.stations[pi + 1] || goalStation;
        // * ich brauch hier nur nach links zu gehen mit den Stations, weil alle veränderungen rechts schon durch andere alternatives gedeckt sind
        let alternatives = getAlternatives(current, prev, next);
        let foundPaths = alternatives.map(a => {
            let p = copyPath(path);
            p.stations[pi] = a;
            return p;
        });
        let newPaths = foundPaths.filter(fp => !DB.paths.some(np => pathsAreEqual(fp, np)));
        DB.paths.push(...newPaths);
        // ! temp
        if (newPaths.length > 0)
            console.log(DB.paths.length);
        if (newPaths.some(p => !checkPossibility(p)) || DB.paths.length > 5000) {
            console.log(newPaths);
            throw new Error("Impossible path found");
        }
        for (const p of newPaths) {
            // * kann man so rekursiv machen, weil ich jedes mal nur nach links gehe,
            // * und wenn es nicht weiter nach links geht, dann gibt es keine newPaths und der Loop hier läuft gar nicht
            makePathVariations(p, Math.max(0, pi - 1));
        }
    }
    return DB.paths;
}
// ! temp
function checkPossibility(path) {
    let s = path.stations;
    for (let i = 0; i < s.length - 1; i++) {
        if (!isInRange(s[i].d, s[i + 1].d))
            return false;
    }
    return true;
}
var cacheCounter = 0;
function getAlternatives(current, prev, next) {
    let hash = current.id + "-" + next.id + "-" + prev.id;
    if (DB.CACHE.getAlternatives.has(hash)) {
        // ! temp
        cacheCounter++;
        if (cacheCounter % 1000 === 0)
            console.log("cachedCounter:", cacheCounter);
        return DB.CACHE.getAlternatives.get(hash);
    }
    let alternatives = [];
    for (let si = current.id - 1; si > prev.id; si--) {
        const alt = DATA.stations[si];
        if (isInRange(alt.d, next.d)) {
            alternatives.push(alt);
        }
        else
            break;
    }
    console.log("hier", hash, alternatives.length);
    DB.CACHE.getAlternatives.set(hash, alternatives);
    return alternatives;
}
function pathToRoute(path) {
    const startStation = { id: -1, d: 0, p: Infinity };
    const goalStation = { id: DATA.stations.length, d: DATA.l, p: 0 };
    let s = path.stations;
    let stops = [];
    // ! tank kann negativ werden
    let tank = DATA.f;
    tank -= distanceToFuel(s[0].d);
    for (let i = 0; i < s.length; i++) {
        const current = s[i];
        const prev = s[i - 1] || startStation;
        const next = s[i + 1] || goalStation;
        // ! distanceAhead wird zu groß -> unmögliche Pfade kommen iwo her -> makePathVariation
        let distanceAhead = next.d - current.d;
        // ! fuelNeeded kann größer als DATA.g werden
        let fuelNeeded = distanceToFuel(distanceAhead);
        console.log(tank, fuelNeeded, distanceAhead);
        let tankUpToFull = next.p > current.p;
        let tankingUp;
        if (tankUpToFull) {
            tankingUp = DATA.g - tank;
            // for next stop
            tank = DATA.g - fuelNeeded;
        }
        else {
            tankingUp = Math.max(fuelNeeded - tank, 0);
            // for next stop
            tank = 0;
        }
        let cost = tankingUp * current.p;
        let stop = { station: current, tankingUp, cost };
        stops.push(stop);
    }
    let totalCost = stops.reduce((c, s) => c + s.cost, 0);
    let route = { stops, cost: totalCost };
    return route;
}
function makeInitialPath() {
    // * Hier muss ne gescheite Kopie hin, da sonst die Reihenfolge in DATA.stations auch reversed wird
    const reversedStations = DATA.stations.slice();
    reversedStations.reverse();
    const stations = [];
    let pos = 0;
    let endOfRange = fuelToDistance(DATA.f);
    let lastStation;
    do {
        lastStation = reversedStations.find(s => s.d < endOfRange);
        stations.push(lastStation);
        pos = lastStation.d;
        endOfRange = pos + fuelToDistance(DATA.g);
        // until end is reachable
    } while (DATA.l > endOfRange);
    return { stations };
}
// === MINI HELPERS ===
function fuelToDistance(fuel) {
    return 100 * fuel / DATA.v;
}
function distanceToFuel(distance) {
    return (distance / 100) * DATA.v;
}
function isReachable(point, from) {
    return (point > from && point <= from + fuelToDistance(DATA.g));
}
function isInRange(start, end) {
    return (end > start && end <= (start + fuelToDistance(DATA.g)));
}
function pathsAreEqual(a, b) {
    for (let pi = 0; pi < a.stations.length; pi++) {
        if (a.stations[pi].id !== b.stations[pi].id) {
            return false;
        }
    }
    return true;
}
function copyPath(path) {
    let newPath = { stations: [] };
    for (let i = 0; i < path.stations.length; i++) {
        newPath.stations[i] = path.stations[i];
    }
    return newPath;
}
function calcSectionCost(stations) {
    let hash = stations.reduce((h, s) => h + s.id + "-", "");
    if (DB.CACHE.calcSectionCost.has(hash))
        return DB.CACHE.calcSectionCost.get(hash);
    let cost = 0;
    for (let i = 0; i < stations.length - 1; i++) {
        let distance = stations[i + 1].d - stations[i].d;
        cost += distanceToFuel(distance) * stations[i].p;
    }
    DB.CACHE.calcSectionCost.set(hash, cost);
    return cost;
}
// I/O
function output(res) {
    let table = res.stops.map((s, i) => ({ "#Stop": i, "#Station": s.station.id, stationPos: s.station.d, getankt: s.tankingUp.toFixed(2), kosten: formatCost(s.cost) }));
    let out = buildOutput(res);
    let html = out.replace(/\n/g, "<br>") + json2table(table);
    document.write(html);
    console.log(out);
    console.table(table);
}
function buildOutput(res) {
    return (`\
=== Ergebnis für ${DATA.filename} ===
Anzahl Stops: ${res.stops.length}
Gesamtkosten: ${formatCost(res.cost)}
Stops:
`);
}
function json2table(json, classes = "") {
    // strongly edited from https://travishorn.com/building-json2table-turn-json-into-an-html-table-a57cf642b84a
    let cols = Object.keys(json[0]);
    let headerRow = '';
    let bodyRows = '';
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    cols.map(col => {
        headerRow += '<th>' + capitalizeFirstLetter(col) + '</th>';
    });
    json.map(row => {
        bodyRows += '<tr>';
        cols.map(colName => {
            bodyRows += '<td>' + row[colName] + '</td>';
        });
        bodyRows += '</tr>';
    });
    return `<table class="${classes}"><thead><tr>${headerRow}</tr></thead><tbody>${bodyRows}</tbody></table>`;
}
function formatCost(cost) {
    return (cost / 100).toFixed(2) + " €";
}
function extractData(file) {
    DATA.filename = file.name;
    let lines = file.body.split(/\r?\n/g);
    DATA.v = Number(lines[0]);
    DATA.g = Number(lines[1]);
    DATA.f = Number(lines[2]);
    DATA.l = Number(lines[3]);
    let stationLines = lines.slice(5, -1);
    let stations = stationLines.map((sl) => {
        let numbers = sl.split(/ +/).map(s => Number(s));
        return { d: numbers[0], p: numbers[1] };
    });
    stations = stations.sort((a, b) => a.d - b.d);
    DATA.stations = stations.map((s, id) => (Object.assign({ id }, s)));
}
//# sourceMappingURL=main.js.map